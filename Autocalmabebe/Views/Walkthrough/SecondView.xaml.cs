﻿using System;
using System.Collections.Generic;
using Autocalmabebe.Services;
using Xamanimation;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Autocalmabebe.Views.Walkthrough
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SecondView : IAnimatedView
    {
   

        public SecondView()
        {
            InitializeComponent();
        }

        public void StartAnimation()
        {
            if (Resources["BackgroundColorAnimation"] is ColorAnimation backgroundColorAnimation)
            {
                backgroundColorAnimation.Begin();
            }

            if (Resources["InfoPanelAnimation"] is StoryBoard infoPanelAnimation)
            {
                infoPanelAnimation.Begin();
            }

            LottieAnimation.Play();
        }
    }
}
