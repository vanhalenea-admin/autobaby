﻿using System;
using System.Collections.Generic;
using Autocalmabebe.Services;
using Xamanimation;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Autocalmabebe.Views.Walkthrough
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FirstView : IAnimatedView
    {
        public FirstView()
        {
            InitializeComponent();
        }

        public void StartAnimation()
        {
            if (Resources["InfoPanelAnimation"] is StoryBoard animation)
            {
                animation.Begin();
            }
        }
    }
}
