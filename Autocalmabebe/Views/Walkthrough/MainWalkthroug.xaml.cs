﻿using System;
using System.Collections.Generic;
using Autocalmabebe.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Autocalmabebe.Views.Walkthrough
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainWalkthroug : ContentPage
    {
        
        private View[] _views;

        public MainWalkthroug()
        {
            InitializeComponent();
            _views = new View[]
         {
                new FirstView(),
                new SecondView(),
                new ThirdView()
         };
            Carousel.ItemsSource = _views;

        }

        private void OnCarouselPositionSelected(object sender, CarouselView.FormsPlugin.Abstractions.PositionSelectedEventArgs e)
        {
            var currentView = _views[e.NewValue];

            if (currentView is IAnimatedView animatedView)
            {
                animatedView.StartAnimation();
            }
        }
    }
}
