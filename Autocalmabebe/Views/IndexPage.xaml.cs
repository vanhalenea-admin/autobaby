﻿
using Autocalmabebe.ViewModels;
using Plugin.AudioRecorder;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;



namespace Autocalmabebe.Views
{
    public partial class IndexPage : ContentPage
    {
        public IndexPage()
        {
            InitializeComponent();
            this.MainInstance = MainViewModel.GetInstance();

            _recoder = new AudioRecorderService
            {
                StopRecordingOnSilence = false,
                StopRecordingAfterTimeout = true,
                AudioSilenceTimeout = TimeSpan.FromSeconds(10),
                TotalAudioTimeout = TimeSpan.FromSeconds(10)


            };

            _player = new AudioPlayer();
            _recoder.AudioInputReceived += _recoder_AudioInputReceived;
            _player.FinishedPlaying += Player_FinishedPlaying;
        }



        private AudioRecorderService _recoder;
        private AudioPlayer _player;
        private MainViewModel MainInstance;


        protected override void OnAppearing()
        {

        }

        private void _recoder_AudioInputReceived(object sender, string e)
        {
            var uri = new Uri(@"/Assets/1.wav", UriKind.Absolute);

            _player.Play(uri.AbsoluteUri);
         
         
        }


        async void Player_FinishedPlaying(object sender, EventArgs e)
        {
            await RecodAudio();
        }
        private async void Button_Clicked(object sender, EventArgs e)
        {
            await RecodAudio();
        }
        private async Task RecodAudio()
        {
            try
            {
                if (!_recoder.IsRecording)
                {
                    this.MainInstance.Index.onButtonClick();
                    await _recoder.StartRecording();
                }
                else
                {
                    this.MainInstance.Index.onButtonClick();
                    await _recoder.StopRecording();
                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

   



    }
}
