﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Autocalmabebe.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterPage : ContentPage
    {
        void OnItemTapped(object sender, ItemTappedEventArgs e)
        { if (e == null) return; // has been set to null, do not 'process' tapped event Debug.WriteLine("Tapped: " + e.Item); 
            ((ListView)sender).SelectedItem = null; 
        }
        public MasterPage()
        {
            InitializeComponent();
        }

    }
}
