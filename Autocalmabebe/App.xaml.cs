
using Autocalmabebe.Views.Walkthrough;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Autocalmabebe
{
    public partial class App : Application
    {

        public static MasterDetailPage masterD { get; set; }

        public App()
        {
            //XamEffects.Effects.Init();
            
            InitializeComponent();
           
         // MainPage = new MainPage();
         MainPage = new MainWalkthroug();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
