﻿using System;
using System.Collections.Generic;
using Autocalmabebe.Models;

namespace Autocalmabebe.Services
{
    public class AppService
    {
       
        public List<MenuItem> getMenuItems(){

            List<MenuItem> listaMenu = new List<MenuItem>();

            var menuItem1 = new MenuItem();
            menuItem1.Descripcion = "Comenzar monitoreo";
            menuItem1.Imagen = "ic_child_care";
            listaMenu.Add(menuItem1);

            var menuItem2 = new MenuItem();
            menuItem2.Descripcion = "Seteo de sonidos";
            menuItem2.Imagen = "ic_hearing";
            listaMenu.Add(menuItem2);

            var menuItem3 = new MenuItem();
            menuItem3.Descripcion = "Configuración";
            menuItem3.Imagen = "ic_settings";
            listaMenu.Add(menuItem3);

            var menuItem4 = new MenuItem();
            menuItem4.Descripcion = "Suscripción";
            menuItem4.Imagen = "ic_shopping_basket";
            listaMenu.Add(menuItem4);

            var menuItem5 = new MenuItem();
            menuItem5.Descripcion = "Acerca de Auto calma bebe";
            menuItem5.Imagen = "ic_error_outline";
            listaMenu.Add(menuItem5);


            return listaMenu;



        }


    }
}
