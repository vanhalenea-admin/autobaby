﻿using System;

using Xamarin.Forms;

namespace Autocalmabebe.Services
{
    public interface IAnimatedView
    {
        void StartAnimation();
    }
}

