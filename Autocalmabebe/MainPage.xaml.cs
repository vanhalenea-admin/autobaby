﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autocalmabebe.Views;
using Xamarin.Forms;

namespace Autocalmabebe
{
    public partial class MainPage : MasterDetailPage
    {
        public MainPage()
        {
            InitializeComponent();
            this.Master = new MasterPage();
            this.Detail =   new NavigationPage(new IndexPage());

            App.masterD = this;
        }
    }
}
