﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Autocalmabebe.Models;
using Autocalmabebe.Services;
using GalaSoft.MvvmLight.Command;

namespace Autocalmabebe.ViewModels
{
    public class MasterPageViewModel : BaseViewModel
    {

        private AppService _appService;

        private bool _isRefreshing;

        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set
            {
                _isRefreshing = value;
                RaisePropertyChanged(() => this.IsRefreshing);
            }
        }

        private List<MenuItem> _menuItems;

        public List<MenuItem> MenuItems
        {
            get { return _menuItems; }
            set
            {
                _menuItems = value;
                RaisePropertyChanged(() => this.MenuItems);
            }
        }


        public ICommand onItemClickCommand
        {
            get
            {
                return new RelayCommand(onItemClick);
            }
        }


        public MasterPageViewModel()
        {
            this._appService = new AppService();
            this.InitComponents();
        }


        public void InitComponents(){

            this._menuItems = this._appService.getMenuItems();
        }

        public void onItemClick(){

            var x = 0;
        }

    }
}

