﻿using System;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;

namespace Autocalmabebe.ViewModels
{
    public class MainWalkthrougViewModel
    {
        public ICommand onItemSkipCommand
        {
            get
            {
                return new RelayCommand(onSkipClick);
            }
        }


        public void onSkipClick()
        {
            Xamarin.Forms.Application.Current.MainPage = new MainPage();
         
        }
        public MainWalkthrougViewModel()
        {
        }
    }
}
