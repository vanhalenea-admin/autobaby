﻿using Java.IO;
using SoundFingerprinting;
using SoundFingerprinting.Audio;
using SoundFingerprinting.Builder;
using SoundFingerprinting.DAO.Data;
using SoundFingerprinting.InMemory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Autocalmabebe.ViewModels
{
    public class MainViewModel
    {
        public IndexViewModel Index { get; set; }
        public MasterPageViewModel Master { get; set; }
        public MainWalkthrougViewModel Walk { get; set; }


        private readonly IModelService modelService = new InMemoryModelService(); // store fingerprints in RAM
        private readonly IAudioService audioService = new SoundFingerprintingAudioService(); // default audio library

        public void StoreAudioFileFingerprintsInStorageForLaterRetrieval(string pathToAudioFile)
        {
            var track = new TrackData("GBBKS1200164", "Adele", "Skyfall", "Skyfall", 2012, 290);

            // store track metadata in the datasource
            var trackReference = modelService.InsertTrack(track);

            // create hashed fingerprints
            var hashedFingerprints = FingerprintCommandBuilder.Instance
                                        .BuildFingerprintCommand()
                                        .From(pathToAudioFile)
                                        .UsingServices(audioService)
                                        .Hash()
                                        .Result;

            // store hashes in the database for later retrieval
            modelService.InsertHashDataForTrack(hashedFingerprints, trackReference);
        }


        public TrackData GetBestMatchForSong(string queryAudioFile)
        {
            int secondsToAnalyze = 10; // number of seconds to analyze from query file
            int startAtSecond = 0; // start at the begining

            // query the underlying database for similar audio sub-fingerprints
            var queryResult = QueryCommandBuilder.Instance.BuildQueryCommand()
                                                 .From(queryAudioFile, secondsToAnalyze, startAtSecond)
                                                 .UsingServices(modelService, audioService)
                                                 .Query()
                                                 .Result;

            return queryResult.BestMatch.Track;
        }


        public MainViewModel()
        {
            instance = this;
            this.Index = new IndexViewModel();
            this.Master = new MasterPageViewModel();
            this.Walk = new MainWalkthrougViewModel();
            this.loadAudiosFingerPrints();


        }

  


        private async void loadAudiosFingerPrints()
        {
            var assembly =  typeof(App).GetTypeInfo().Assembly;
            var resourcePath = "";

            List<string> resourceNames = new List<string>(assembly.GetManifestResourceNames());

            resourcePath = resourcePath.Replace(@"/", ".");
            resourcePath = resourceNames.FirstOrDefault(r => r.Contains(resourcePath));

      



        }



        private static MainViewModel instance;

        public static MainViewModel GetInstance()
        {
            if (instance == null)
                return new MainViewModel();

            return instance;
        }
    }
}
