﻿
using GalaSoft.MvvmLight.Command;
using SoundFingerprinting;
using SoundFingerprinting.Audio;
using SoundFingerprinting.InMemory;
using System.Windows.Input;



namespace Autocalmabebe.ViewModels
{
    public class IndexViewModel : BaseViewModel
    {

        private readonly IModelService modelService = new InMemoryModelService(); // store fingerprints in RAM
        private readonly IAudioService audioService = new SoundFingerprintingAudioService(); // default audio library




        private bool _isActive;

        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                RaisePropertyChanged(() => this.IsActive);
            }
        }

        private bool _isVisibleBotonVerde;

        public bool IsVisibleBotonVerde
        {
            get { return _isVisibleBotonVerde; }
            set
            {
                _isVisibleBotonVerde = value;
                RaisePropertyChanged(() => this.IsVisibleBotonVerde);
            }
        }

        private bool _isVisibleBotonRojo;

        public bool IsVisibleBotonRojo
        {
            get { return _isVisibleBotonRojo; }
            set
            {
                _isVisibleBotonRojo = value;
                RaisePropertyChanged(() => this.IsVisibleBotonRojo);
            }
        }

        private string _textoMonitoreo;

        public string TextoMonitoreo
        {
            get { return _textoMonitoreo; }
            set
            {
                _textoMonitoreo = value;
                RaisePropertyChanged(() => this.TextoMonitoreo);
            }
        }


        public ICommand onButtonClickCommand
        {
            get
            {
                return new RelayCommand(onButtonClick);
            }
        }





 


        public async void onButtonClick()
        {
            if (!this.IsActive)
            {
                this.IsActive = true;
                this.IsVisibleBotonVerde = true;
                this.IsVisibleBotonRojo= false;
                this.TextoMonitoreo = "MONITOREANDO...";

            }
            else
            {
                this.IsActive = false;
                this.IsVisibleBotonVerde = false;
                this.IsVisibleBotonRojo = true;
                this.TextoMonitoreo = "PULSA PARA MONITOREAR";
            }

        }



        public void Init()
        {
            this.IsVisibleBotonRojo = true;
            this.IsVisibleBotonVerde = false;
            this.TextoMonitoreo = "PULSA PARA MONITOREAR";

        }

        public IndexViewModel()
        {
            this.Init();
        }
    }
}

