﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Autocalmabebe.Common
{
    public class AppConstant
    {
        public const string RUTA_BOTON_ROJO = "animation_circle_red.json";
        public const string RUTA_BOTON_VERDE = "animation_circle_active.json";
    }
}
